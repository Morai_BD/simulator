#!/usr/bin/env python
from pprint import pprint

import can
import sys
import time
import threading
from struct import *
from datetime import datetime
from tabulate import tabulate
import os
import csv
import mscenario
LATERALALIVE=0
LONGALIVE=0
LongCmd=100


InputTurnSignalCmd=0
InputGear=0
InputMode=0
InputSTEER=0
InputACCEL=0
InputBRAKE=0

OutputTurnSignal=0
OutputGear=0
OutputACCEL=0
OutputBrake=0
OutputSTEER=0
OutputMode=0



can_list = []
tc_list = []

# 데이터 저장을 위한 경로 및 파일명 지정
# 데이터 헤더 추가
filename = datetime.now().strftime('%H_%M_%S') +'.csv'
f= open(filename,'w',newline='')
wr=csv.writer(f)
wr.writerow(["Time(s)","InputTurnSignal(0~2)","OutputTurnSignal(0~2)","InputGear(0~3)","OutputGear(0~3)","InputCtrlMode(0~1)","OutputCtrlMode(0~1)","InputSteer(-450~450)","OutputSteer(-450~450)","InputThrottle(0~100)","OutputThrottle(0~100)","InputBrake(0~100)","OuputBrake(0~100)"])




startTime=None
def SearchDevice():
    global can_list
    devices = list(can.detect_available_configs())
    # devices = list(can.CAN.
    if len(devices) < 1:
        print('\n====There are not any devices to connect====')
        sys.exit()
    print('\n====detected devices list====')
    
    for item in devices:
        if(item['interface'] == 'kvaser'):
            try:
                bus = can.interface.Bus(bustype=item['interface'], channel=item['channel'], bitrate=1000000)
                can_list.append(bus)
                
                print('inferface : {0}, channel : {1}, info : {2}'.format({item['interface']}, {item['channel']}, {bus.channel_info}))
            except:
                print("Can't Search Kvaser CAN Device")
                
def TestMsg(InputTurnSignalCmd,InputGear,InputMode,InputSTEER,InputACCEL,InputBRAKE):
    global LATERALALIVE
    global LONGALIVE
    global startTime
    if startTime==None:
        startTime=datetime.now()
    LateralCmdMode=0
    LongCmdMode=0

    TurnModeCmd=1    # 1 : ON
    TurnSignalCmd=InputTurnSignalCmd  # 0 : OFF  , 1: RIGHT , 2 :LEFT 
    HazardSignalCmd=0  # 1 : ON

    msg = can.Message(arbitration_id=261, data=[TurnSignalCmd, HazardSignalCmd, 0, 0, 0, 0, TurnModeCmd, 0], is_extended_id=True) 
    tc_list.append(msg)

    GearModeCmd=1
    Gear=0 # 1 : P , 2: R , 4 : N , 8 : D
    if InputGear == 0 :
        Gear=1  
    if InputGear == 1 :
        Gear=2  
    if InputGear == 2 :
        Gear=4  
    if InputGear == 3 :
        Gear=8  
    msg = can.Message(arbitration_id=260, data=[Gear, 0, 0, 0, 0, 0, GearModeCmd, 0], is_extended_id=True)  # 기어 제어 메시지
    tc_list.append(msg)

    if InputMode == 0:
        LateralCmdMode=0
        LongCmdMode=0
    else :
        LateralCmdMode=1
        LongCmdMode=1

   
    STEER= -InputSTEER*10
    STEER_L = STEER & 0xff
    STEER_H = (STEER & 0xff00) >>8

    if LATERALALIVE >= 15:
        LATERALALIVE=0
    LATERALALIVE+=1
    # print(STEER_L,STEER_H)
    msg = can.Message(arbitration_id=259, data=[STEER_L, STEER_H, 0, 0, 0, 0, LateralCmdMode, LATERALALIVE], is_extended_id=True) #횡방향 제어 메시지
    tc_list.append(msg)
    
    AEB=1
    AEBCmd=InputBRAKE
    LongCmd=InputACCEL
    LongCmd_L=LongCmd & 0xff
    LongCmd_H=(LongCmd & 0xff00) >>8
    
    if LONGALIVE >= 15:   # ALIVE COUNT 업데이트, ALIVE 카운트는 메시지를 보낼 때마다 1씩 증가
        LONGALIVE=0
    LONGALIVE+=1

    msg = can.Message(arbitration_id=258, data=[LongCmd_L, LongCmd_H, AEBCmd, 0, AEB, 0, LongCmdMode, LONGALIVE], is_extended_id=True) #종방향 제어 메시지
    tc_list.append(msg)

                
def InputKey():
    key = int(input('Select Channel\n'))
    return key


# def Scenario(diff):
#     TurnSignalCmd=InputTurnSignalCmd
#     Gear=InputGear
#     Mode=InputMode
#     Steer=InputSTEER
#     Accel=InputACCEL
#     Brake=InputBRAKE
#     if diff > 2 :
#         TurnSignalCmd=1
#     if diff > 4 :
#         TurnSignalCmd=0
#     if diff > 6 :
#         TurnSignalCmd=2
#     if diff > 8 :
#         TurnSignalCmd=0
#     if diff > 10 :
#         Gear=1
#     if diff > 12 :
#         Gear=2
#     if diff > 14 :
#         Gear=3
#     if diff > 16 :
#         Mode=1
#     if diff > 18 :
#         Mode=0
#     if diff > 20 :
#         Mode=1
#     if diff > 22 :
#         Accel=25
#     if diff > 24 :
#         Accel=50
#     if diff > 26 :
#         Accel=75
#     if diff > 28 :
#         Accel=100
#     if diff > 30 :
#         Accel=0
#     if diff > 32 :
#         Brake=25
#     if diff > 34 :
#         Brake=50
#     if diff > 36 :
#         Brake=75
#     if diff > 38 :
#         Brake=100
#     if diff > 40 :
#         Brake=0
#     if diff > 42 :
#         Accel=20

#     if diff > 44 :
#         Steer=-15
#     if diff > 46 :
#         Steer=-30
#     if diff > 48 :
#         Steer=0
#     if diff > 50 :
#         Steer=15
#     if diff > 52 :
#         Steer=30
#     if diff > 54 :
#         Steer=0
#     if diff > 56 :
#         Accel=0
#         Brake=100
#     return [TurnSignalCmd,Gear,Mode,Steer,Accel,Brake]


def turnsignalConv(data):
    if data==0 :
        return "OFF"
    elif data==1 :
        return "Right"
    elif data==2 :
        return "Left"


def gearConv(data):
    if data==0 :
        return 'P'
    elif data==1:
        return 'R'
    elif data==2:
        return 'N'
    elif data==3:
        return 'D'


def ctrlModeConv(data):
    if data==0 :
        return "ManualMode"
    elif data==1 :
        return "AutoMode"


def SendMsg(channel):
    global can_list, tc_list
    global InputTurnSignalCmd, InputGear, InputMode, InputSTEER, InputACCEL, InputBRAKE
    while True :

        TestMsg(InputTurnSignalCmd,InputGear,InputMode,InputSTEER,InputACCEL,InputBRAKE)   # 송신할 메시지 생성 INPUT 값 적용
        for i in tc_list:
            try:
                can_list[channel].send(i)    # 메시지 송신
            except can.CanError:
                print("Message NOT sent")

        now= datetime.now()
        diff = (now-startTime).total_seconds()      
        scenarioOut=mscenario.Scenario(diff)        #시나리오 업데이트
        InputTurnSignalCmd=scenarioOut[0]
        InputGear=scenarioOut[1]
        InputMode=scenarioOut[2]
        InputSTEER=scenarioOut[3]
        InputACCEL=scenarioOut[4]
        InputBRAKE=scenarioOut[5]

        # 데이터 저장
        wr.writerow([diff,
                    InputTurnSignalCmd,OutputTurnSignal,
                    InputGear,OutputGear,
                    InputMode,OutputMode,
                    InputSTEER,OutputSTEER,
                    InputACCEL,OutputACCEL,
                    InputBRAKE,OutputBrake])



        # 터미널 창에 데이터 출력을 위한 부분
        table=[['TurnSignal',turnsignalConv(InputTurnSignalCmd),turnsignalConv(OutputTurnSignal)] , 
               ['Gear',gearConv(InputGear),gearConv(OutputGear)],
               ['CtrlMode',ctrlModeConv(InputMode),ctrlModeConv(OutputMode)],
               ['Throttle',str(InputACCEL),str(int(OutputACCEL))],
               ['Brake',str(InputBRAKE),str(int(OutputBrake))],
               ['Steer',str(InputSTEER),str(int(OutputSTEER))]]
               
        dict_test = {
            'TimeStamp': ['TimesStamp : '+str(diff)]
        }
        print(tabulate(dict_test, tablefmt='psql',showindex='keys'))
        print(tabulate(table, headers=['Control','Input','Output'], tablefmt='psql'))

        tc_list.clear()
        time.sleep(0.02)    # 데이터 송신 주기 50hz로 설정
        os.system('cls')    # 터미널 창 Clear




def RecvMsg(channel):
    global can_list
    global LongCmd
    global OutputMode
    global OutputTurnSignal
    global OutputGear
    global OutputACCEL
    global OutputBrake
    global OutputSTEER
    prev_time= datetime.now()
    try:
        while True:
            msg = can_list[channel].recv(1)

            if msg !=None : 
                current_time=datetime.now()
                diff=(current_time-prev_time).total_seconds() # 데이터 수신 주기
                if(msg.arbitration_id == 512):  # 각 CAN 메시지 별로 데이터 처리 (512, 513, 515) 총 3종류의 메시지가 있음
                    OutputMode=msg.data[0] >>1 & 0b11  
                    if OutputMode ==3 :
                        OutputMode=1                    #제어 모드 상태값
                    OutputTurnSignal =0
                    RightTurn=msg.data[1]>>3 & 0b1      #왼쪽 방향지시등 상태값
                    LeftTurn=msg.data[1]>>4 & 0b1       #오른쪽 방향지시등 상태값

                    if RightTurn==1 :
                        OutputTurnSignal=1
                    if LeftTurn==1 :
                        OutputTurnSignal=2
                    OutputGear = msg.data[2] &0b1111    # 기어 상태값
                    
                    prev_time=current_time
                if(msg.arbitration_id == 513):
                    OutputACCEL=unpack("h",msg.data[0:2])[0]/32   # ACCEL Pedal 상태값
                    OutputBrake= unpack("h",msg.data[2:4])[0]/32  # BRAKE Pedal 상태값
                if(msg.arbitration_id == 515):
                    OutputSTEER=(-1)*unpack("h",msg.data[3:5])[0]/10  # 핸들 각도 상태값

                
    except KeyboardInterrupt:
        pass
        
        
if __name__ == "__main__":  # 메인문
    SearchDevice()   # 연결되어 있는 CAN 장치 탐색
    
    channel = 1  # 통신할 CAN 장치 선택

   

    send_thread = threading.Thread(target=SendMsg, args=(channel,))  #데이터 송신 전용 쓰래드 생성
    send_thread.daemon = True 
    send_thread.start() # 송신 전용 쓰래드 시작
    
   
    RecvMsg(channel) # 메인쓰래드 , 시뮬레이터로부터 상태 데이터 수신하는 함수 무한루프
    f.close() 
    sys.exit()